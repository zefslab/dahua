﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using System.Threading;

namespace Example
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
     public struct SNAP_PARAMS
    {
        public uint Channel;                // Snapshot channel
        public uint Quality;                // Image quality:level 1 to level 6
        public uint ImageSize;              // Video size£»0£ºQCIF£¬1£ºCIF£¬2£ºD1
        public uint mode;                   // Snapshot mode£»0£ºrequest one frame£¬1£ºsend out requestion regularly£¬2£º Request consecutively
        public uint InterSnap;              // Time unit is second.If mode=1, it means send out requestion regularly. The time is valid.
        public uint CmdSerial;              // Request serial number
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public uint[] Reserved;
    }

     public struct LPNET_DEVICEINFO
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public byte[] sSerialNumber;    // SN
                                                //            byte sSerialNumber;
        public byte byAlarmInPortNum;       // DVR alarm input amount
        public byte byAlarmOutPortNum;      // DVR alarm output amount
        public byte byDiskNum;              // DVR HDD amount 
        public byte byDVRType;              // DVR type.Please refer to DHDEV_DEVICE_TYPE
        public byte byChanNum;              // DVR channel ammount 
    }

    [StructLayout(LayoutKind.Sequential)]
    public class NET_DEVICEINFO
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public byte[] sSerialNumber;
        public byte byAlarmInPortNum;
        public byte byAlarmOutPortNum;
        public byte byDiskNum;
        public byte byDVRType;
        public byte byChanNum;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct NET_TIME
    {
        private uint dwYear;
        private uint dwMonth;
        private uint dwDay;
        private uint dwHour;
        private uint dwMinute;
        private uint dwSecond;
    }
    public class PlatformInvokeTest
    {
        //Изменил тип int на uint
        public static long lLogin;

        public delegate void fDisConnect(long lLoginID, IntPtr pchDVRIP, long nDVRPort, uint dwUser);

        public delegate void fHaveReConnect(long lLoginID, IntPtr pchDVRIP, long nDVRPort, uint dwUser);

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool CLIENT_Init(fDisConnect cbDisConnect, uint dwUser);

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CLIENT_SetAutoReconnect(fHaveReConnect cbHaveReconnt, uint dwUser);

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern long CLIENT_Login(string pchDVRIP, ushort wDVRPort, string pchUserName, string pchPassword,
             NET_DEVICEINFO lpDeviceInfo, ref long error);
        /// <summary>
        /// Подготовительный метод для захвата картинки
        /// </summary>
        /// <param name="lLoginID"></param>
        /// <param name="nChannelID"></param>
        /// <param name="hWnd">#define HWND void* 
        /// window handle,when value is 0, do not decode or display data </param>
        /// <returns>Begin real-time monitor </returns>
        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern long CLIENT_RealPlay(long lLoginID, int nChannelID, uint hWnd);

        /// <summary>
        /// Важный метод для захвата картинки
        /// </summary>
        /// <param name="hPlayHandle">hPlayHandle is monitor or playback handle</param>
        /// <param name="pchPicFileName"></param>
        /// <returns></returns>
        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool CLIENT_CapturePicture(long hPlayHandle, [MarshalAs(UnmanagedType.LPStr)]string pchPicFileName);


        [DllImport("dhnetsdk.dll")]
        private static extern bool CLIENT_SnapPicture(long lLoginID, SNAP_PARAMS par);

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CLIENT_GetDevConfig(long loginId,uint command,int channel,
            out NET_TIME buffer,out uint bufferSize,IntPtr lpBytesReturned,int waittime = 500);

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool CLIENT_StopRealPlay(long lRealHandle);
        
        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool CLIENT_Logout(long lID);

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CLIENT_Cleanup();

        [DllImport("dhnetsdk.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint CLIENT_GetLastError();

        public static void fDisConnectMethod(long lLoginID, IntPtr pchDVRIP, long nDVRPort, uint dwUser)
        {
            System.Console.WriteLine("Disconnect");
            return;
        }

        public static void fHaveReConnectMethod(long lLoginID, IntPtr pchDVRIP, long nDVRPort, uint dwUser)
        {
            System.Console.WriteLine("Reconnect success");
            return;
        }
       

      


        public  static void Main()
        {
            fDisConnect fDisConnecthandler = fDisConnectMethod;
            fHaveReConnect fHaveReConnecthandler = fHaveReConnectMethod;
            NET_DEVICEINFO deviceinfo = new NET_DEVICEINFO();
            IntPtr iRet = new IntPtr(0);
            long ret = 0;
            ushort port = 37777;
            var result = CLIENT_Init(fDisConnectMethod, 0);
            CLIENT_SetAutoReconnect(fHaveReConnecthandler, 0);

            lLogin = CLIENT_Login("78.188.168.175", port, "admin", "admin", deviceinfo, ref ret);

            if (lLogin <= 0)
                Console.WriteLine("Login device failed");
            else
            {
                Console.WriteLine("Login device successful");

                //var file = new FileInfo("testPicture.bmp");
                //file.Create();

                SNAP_PARAMS snap_params = new SNAP_PARAMS();
                
                bool b = CLIENT_SnapPicture(lLogin, snap_params);
                

                var lRealHandle = CLIENT_RealPlay(lLogin, 0, 0);

                var resultCapture = CLIENT_CapturePicture(lRealHandle, "testPicture.bmp");

                var error = CLIENT_GetLastError()-2147483648; //ошибка 4-invalid handle

                CLIENT_StopRealPlay(lRealHandle);

                var logout = CLIENT_Logout(lLogin);

                CLIENT_Cleanup();

                //byte[] byteout = new byte[20];
                //const int t = 500;
                //IntPtr BytesReturned;
                //BytesReturned = IntPtr.Zero;
                //const uint c = 8;
                //NET_TIME nt;
                //uint sizeofnt = (uint)Marshal.SizeOf(typeof(NET_TIME));
                //if (!CLIENT_GetDevConfig(lLogin, c, 0, out nt, out sizeofnt, BytesReturned, t))
                //{
                //    uint gle = CLIENT_GetLastError();
                //    Console.WriteLine("getDevConfig failed");
                //}
                //CLIENT_Logout(lLogin);
                //CLIENT_Cleanup();
            }
        }
    }
}